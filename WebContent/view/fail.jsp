<%@page import="vo.MessageVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>登录失败</title>
<style type="text/css">
h3{
	text-align: center;
}
</style>
</head>
<%
MessageVO vo = (MessageVO) request.getAttribute("vo");
%>
<body>
<h3>登录失败</h3>
<table>
		<thead>
			<tr>
				<td>信息</td>
				<td>用户名</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><%=vo.getMessage() %></td>
				<td><%=vo.getUsername()%></td>
			</tr>
		</tbody>
	</table>
</body>
</html>