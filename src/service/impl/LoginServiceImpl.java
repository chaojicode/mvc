package service.impl;

import service.LoginService;
import vo.MessageVO;

public class LoginServiceImpl implements LoginService{

	@Override
	public MessageVO isLogin(String username, String password) {
		MessageVO vo = new MessageVO();
		if("admin".equals(password)) {
			vo.setMessage("登录成功");
			vo.setUsername(username);
			vo.setStatus("success");
		}else {
			vo.setMessage("登录失败");
			vo.setUsername(username);
			vo.setStatus("fail");
		}
		return vo;
	}

}
