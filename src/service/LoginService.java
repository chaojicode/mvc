package service;

import vo.MessageVO;

public interface LoginService {
	MessageVO isLogin(String username, String password);

}
