package business;

import mvc.form.Form;

public class LoginForm extends Form implements Comparable{
	
	public LoginForm() {
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}
	
	private String username;
	private String password;
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "username="+username+"||password="+password;
	}

}
