package business;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mvc.action.Action;
import mvc.form.Form;
import mvc.xml.XmlBean;
import service.LoginService;
import service.impl.LoginServiceImpl;
import vo.MessageVO;

public class LoginAction implements Action{

	@Override
	public String execute(HttpServletRequest request, Form form, Map<String, String> map) {
		LoginForm loginForm = (LoginForm) form;
		LoginService loginService = new LoginServiceImpl();
		MessageVO vo = loginService.isLogin(loginForm.getUsername(), loginForm.getPassword());
		request.setAttribute("vo", vo);
		return map.get(vo.getStatus());
	}

}
