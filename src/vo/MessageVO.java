package vo;

/**
 * vo的作用是隔离层
 * @author jiangchao
 *
 */
public class MessageVO {
	public MessageVO() {
	}

	private String status;
	private String message;
	private String username;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "username="+this.username+"||message="+this.message;
	}

}
