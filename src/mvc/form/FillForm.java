package mvc.form;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

public class FillForm {
	private FillForm() {

	};

	public static Form fill(HttpServletRequest request, String formClassPath)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class clazz = Class.forName(formClassPath);
		Form form = (Form) clazz.newInstance();
		Field[] f_array = clazz.getDeclaredFields();
		for (Field field : f_array) {
			field.setAccessible(true);
			field.set(form, request.getParameter(field.getName()));
			field.setAccessible(false);
		}
		return form;
	}

}
