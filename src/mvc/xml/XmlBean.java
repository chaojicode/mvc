package mvc.xml;

import java.util.Map;

/**
 * mvc框架配置文件对象
 * 
 * @author jiangchao
 *
 */
public class XmlBean {
	public XmlBean() {
	}

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 表单对象类路径
	 */
	private String formClass;

	/**
	 * 业务执行单元类路径
	 */
	private String actionType;

	/**
	 * 访问路径
	 */
	private String actionPath;

	/**
	 * 跳转url
	 */
	private Map<String, String> forward;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormClass() {
		return formClass;
	}

	public void setFormClass(String formClass) {
		this.formClass = formClass;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActionPath() {
		return actionPath;
	}

	public void setActionPath(String actionPath) {
		this.actionPath = actionPath;
	}

	public Map<String, String> getForward() {
		return forward;
	}

	public void setForward(Map<String, String> forward) {
		this.forward = forward;
	}

}
