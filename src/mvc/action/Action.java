package mvc.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mvc.form.Form;
import mvc.xml.XmlBean;

/**
 * 定义业务操作单元需要执行的方法，主要目的是获取具体的填充表单对象
 * @author jiangchao
 *
 */
public interface Action {
	public String execute(HttpServletRequest request, Form form, Map<String, String> map);
}
