package mvc;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.jdom2.JDOMException;

import mvc.xml.XmlBean;
import mvc.xml.XmlBeanParse;

public class ActionListener implements ServletContextListener{

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("系统注销了");
		
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		ServletContext  servletContext = sce.getServletContext();
//		获取配置文件路径
		String configPath = servletContext.getInitParameter("config-path");
//		获取项目所在服务器的真实路径
		String serverPath = servletContext.getRealPath("\\");
		Map<String, XmlBean> map = null;
		try {
//			执行解析方法
			map = XmlBeanParse.parse(serverPath+configPath);
		} catch (JDOMException e) {
			e.printStackTrace();
			System.out.println("严重：解析异常");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("严重：IO异常");
		}
//		将对象利用servlet上下文存储并交由ActionServlet中处理
		servletContext.setAttribute("structs", map);
		
		System.out.println("系统加载完成");
		
	}

}
